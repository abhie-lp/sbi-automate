from sys import exit
from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, InvalidElementStateException

options = Options()
options.add_experimental_option("debuggerAddress", "localhost:9876")
driver: WebDriver = Chrome(options=options,
                service=Service(executable_path="/home/abhishek/Documents/python/chromedriver"))

windows = driver.window_handles
driver.switch_to.window(windows[0])
pages = driver.find_element(By.CLASS_NAME, "status").text.split(" of ", 1)
buttons_done = False
while pages[0] != pages[1]:
    try:
        driver.switch_to.frame(
            driver.find_element(By.ID, "textcontent")
        )
        # Click each part present
        try:
            click_here = driver.find_element(By.CLASS_NAME, "textnote")
        except Exception:
            click_here = driver.find_element(By.CLASS_NAME, "clicktext")
        if click_here.is_displayed():
            print("Click here now visible")
            click_here = click_here.text
            if "Next" in click_here:
                print("Found Next button to click")
                driver.find_element(By.ID, "continue1").click()
                sleep(4)
                continue
            elif "button" in click_here and not buttons_done:
                print("Found buttons to click")
                for ind, btn in enumerate(driver.find_elements(By.CSS_SELECTOR, '[id^="showdiv"]')):
                    print("Click btn", ind)
                    btn.click()
                    sleep(2)
                    driver.find_elements(By.CSS_SELECTOR, '[id^="Close"]')[ind].click()
                buttons_done = True
    except (NoSuchElementException, InvalidElementStateException) as e:
        print(e.msg)
    finally:
        driver.switch_to.default_content()
    try:
        if driver.find_element(By.CLASS_NAME, "clickNext").is_displayed():
            driver.find_element(By.ID, "next").click()
            buttons_done = False
            print("Next it is")
            sleep(10)
    except NoSuchElementException:
        sleep(4)
    else:
        sleep(4)
    pages = driver.find_element(By.CLASS_NAME, "status").text.split(" of ", 1)
    print(pages)

print("Initiate exit")
while True:
    if driver.find_element(By.ID, "click_nextcontinue").is_displayed():
        print("Click exit")
        driver.find_element(By.ID, "exit_btn").click()
        sleep(1)
        driver.find_element(By.ID, "onExitYes").click()
        break
    sleep(3)

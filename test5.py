from sys import exit
from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, InvalidElementStateException
from logging import StreamHandler, getLogger, Formatter, INFO
from logging.handlers import RotatingFileHandler

log = getLogger(__name__)
log.setLevel(INFO)
stream_handler = StreamHandler()
file_handler = RotatingFileHandler("log.log", maxBytes=5*1024*1024, backupCount=2)
file_handler.namer = lambda filename: filename.replace(".log", "") + ".log"
formatter = Formatter(
    '%(asctime)s [%(levelname)s][%(lineno)d] %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S"
)
file_handler.setFormatter(formatter)
stream_handler.setFormatter(formatter)
log.addHandler(stream_handler)
log.addHandler(file_handler)

def has_element(self, selector, string):
    try:
        return self.find_element(selector, string)
    except NoSuchElementException:
        return False


WebElement.has_element = has_element
WebDriver.has_element = has_element

options = Options()
options.add_experimental_option("debuggerAddress", "127.0.0.1:9876")
driver: WebDriver = Chrome(options=options,
                service=Service(executable_path="/home/abhishek/Documents/python/sbi-automate/chromedriver"))
print("window")
driver.switch_to.window(driver.window_handles[-1])
print("iframe")
driver.switch_to.frame(driver.find_element(By.ID, "textcontent"))

array = driver.execute_script('return ansArray')
print(array)
from sys import exit
from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, InvalidElementStateException

options = Options()
options.add_experimental_option("debuggerAddress", "localhost:9876")
driver: WebDriver = Chrome(options=options,
                service=Service(executable_path="/home/abhishek/Documents/python/chromedriver"))

driver.get("https://egyanshala.sbi.co.in/egyanodaya/user_home")
try:
    sleep(2)
    driver.find_element(By.ID, "modala").find_element(By.CSS_SELECTOR, "button.btn-danger").click()
except NoSuchElementException:
    try:
        # Enter username, password and captcha
        driver.find_element(By.ID, "username").send_keys(8536244)
        driver.find_element(By.ID, "password").send_keys("Hrms^2022")
        driver.find_element(By.ID, "valiIpt").send_keys(input("Enter captcha: "))
        driver.find_element(By.ID, "llll").click()
    except NoSuchElementException:
        exit(1)
    try:
        driver.find_element(By.ID, "navbarDropdownMenuLink")
        driver.find_element(By.CLASS_NAME, "img.sbi_logo1").click()
        sleep(2)
    except NoSuchElementException:
        exit(1)
driver.find_element(By.ID, "modala").find_element(By.CSS_SELECTOR, "button.btn-danger").click()
sleep(2)
driver.find_element(By.CSS_SELECTOR, ".coursecont a").click()
sleep(3)
driver.find_element(By.CSS_SELECTOR, ".slider.round").click()
sleep(2)

for course_btns in driver.find_elements(By.CLASS_NAME, "butt"):
    course_btns.click()
    sleep(2)
    driver.find_element(By.CSS_SELECTOR, "center .btn").click()
    driver.switch_to.window(driver.window_handles[-1])
    for box in driver.find_elements(By.CLASS_NAME, "img1"):
        box.click()
        driver.switch_to.window(driver.window_handles[-1])
        sleep(4)
        pages = driver.find_element(By.ID, "pageNum").text.split(" / ", 1)
        tabs_done = False
        pie_done = False
        while pages[0] != pages[1]:
            try:
                # Click each part present
                click_here = driver.find_element(By.ID, "text_e").text
                if "Next" in click_here:
                    print("Found Next button to click")
                    driver.find_element(By.ID, "continue1").click()
                    sleep(4)
                    continue
                elif "pie" in click_here and not pie_done:
                    print("Found Pie to click")
                    for ind, tab in enumerate(driver.find_elements(By.CSS_SELECTOR, '[id^="mybtn"]')):
                        print("Click pie", ind)
                        tab.click()
                        sleep(3)
                    pie_done = True
                elif "tab" in click_here and not tabs_done:
                    print("Found tabs to click")
                    for ind, tab in enumerate(driver.find_elements(By.CSS_SELECTOR, '[id^="mybtn"]')):
                        print("Click tab", ind)
                        tab.click()
                        sleep(2)
                    tabs_done = True
            except (NoSuchElementException, InvalidElementStateException):
                pass
            try:
                if driver.find_element(By.ID, "click_nextcontinue").is_displayed():
                    driver.find_element(By.ID, "next_btn").click()
                    pie_done, tabs_done = True, True
                    print("Next it is")
                    sleep(10)
            except NoSuchElementException:
                sleep(4)
            else:
                sleep(4)
            pages = driver.find_element(By.ID, "pageNum").text.split(" / ", 1)
            print(pages)

        print("Initiate exit")
        while True:
            if driver.find_element(By.ID, "click_nextcontinue").is_displayed():
                print("Click exit")
                driver.find_element(By.ID, "exit_btn").click()
                sleep(1)
                driver.find_element(By.ID, "onExitYes").click()
                break
            sleep(3)


while True:
    try:
        driver.find_element(By.CSS_SELECTOR, ".pagination a[rel='next']")
    except NoSuchElementException:
        exit(1)
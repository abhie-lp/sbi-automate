from logging import StreamHandler, getLogger, Formatter, INFO, Logger
from logging.handlers import RotatingFileHandler
from multiprocessing import Process
from socket import socket
from subprocess import run
from time import sleep
from typing import TextIO

from selenium.webdriver import Chrome
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, \
    InvalidElementStateException, WebDriverException

log: Logger = getLogger(__name__)
log.setLevel(INFO)
stream_handler: StreamHandler[TextIO] = StreamHandler()
file_handler: RotatingFileHandler = RotatingFileHandler("log.log", maxBytes=5*1024*1024, backupCount=2)
file_handler.namer = lambda filename: filename.replace(".log", "") + ".log"
formatter: Formatter = Formatter(
    '%(asctime)s [%(levelname)s][%(lineno)d] %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S"
)
file_handler.setFormatter(formatter)
stream_handler.setFormatter(formatter)
log.addHandler(stream_handler)
log.addHandler(file_handler)

port = lambda: (sock := socket(), sock.bind(("", 0)), sock.getsockname())[-1]
port = port()[-1]
url: Literal['https://egyanshala.sbi.co.in/egyanodaya/user_home'] = "https://egyanshala.sbi.co.in/egyanodaya/user_home"
log.info("Starting Chrome on port %d with url %s", port, url)
process = Process(
    target=run, name="google-chrome", daemon=True,
    args=(["google-chrome-stable", "--new-window", f"--remote-debugging-port={port}", url],)
)
process.start()

log.info("Connect to webdriver")
options = Options()
options.add_experimental_option("debuggerAddress", f"localhost:{port}")
driver: WebDriver = Chrome(options=options,
                service=Service(executable_path="/home/abhishek/Documents/python/chromedriver"))
log.info("Page title is %s", driver.title)


def has_element(self: WebElement, selector: By, string: str) -> WebElement | bool:
    """Check if element exists"""
    try:
        return self.find_element(selector, string)
    except WebDriverException:
        return False


WebElement.has_element = has_element
WebDriver.has_element = has_element


def move_to_next_course() -> None:
    log.info("Close current course")
    if driver.has_element(By.CLASS_NAME, "closeImg"):
        driver.execute_script("document.querySelector('div.closeImg').click()")
    else:
        driver.find_element(By.ID, "p").click()
        sleep(1)
        driver.find_element(By.ID, "onExitYes").click()
    log.info("Switch to main window")
    if len(driver.window_handles) != 1:
        driver.close()
    sleep(2)
    driver.switch_to.window(driver.window_handles[-1])
    log.info("Go back to course list")
    driver.execute_script("document.querySelector('a.button3').click()")
    sleep(1)



def module_selector_1() -> None:
    # Get all module boxes
    module_boxes: list[WebElement] = driver.find_element(By.CLASS_NAME, "moduleContent")\
        .find_elements(By.CSS_SELECTOR, "button,div")
    for i in range(0, len(module_boxes), 2):
        module_name: str = module_boxes[i].find_element(By.TAG_NAME, "font").get_attribute("textContent").strip()
        # Ignore the ones with tick mark
        if module_boxes[i+1].is_displayed():
            log.info("Module %s done", module_name)
        else:
            log.info("Starting module %s", module_name)
            module_boxes[i].click()
            sleep(2)
            log.info("Switch to module content window")
            driver.switch_to.window(driver.window_handles[-1])


def module_selector_2() -> None:
    pass


course_ignore_list, current_course = set(), ""
goto_checkpt1, goto_checkpt2, all_done = False, False, False

log.info("Start checkpoint 1")
while True:
    if "/login" in driver.current_url:
        log.info("Not logged in. Redirected to %s page", driver.current_url)
        log.info("Wait till logged in")
        
        while driver.current_url != url:
            try:
                sleep(3)
            except KeyboardInterrupt:
                exit(1)
    if "/user_course/" not in driver.current_url:
        sleep(2)
        log.info("Close the modal")
        driver.find_element(By.ID, "modala").find_element(By.CSS_SELECTOR, "button.btn-danger").click()
        log.info("Click View to go to courses page")
        driver.find_element(By.CSS_SELECTOR, ".coursecont a").click()
        wait(driver, 15).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, ".slider.round"))
        )
    
    log.info("Reached checkpoint 2")
    sleep(3)
    while True:
        # log.info("Click slider to show uncompleted")
        # driver.find_element(By.CSS_SELECTOR, ".slider.round").click()
        # sleep(4)
        log.info("Get all modules that are currently shown")
        modules: list[WebElement] = [module 
                   for module in driver.find_elements(By.CLASS_NAME, "modulebox")
                   if module.has_element(By.CLASS_NAME, "progress-bar")]
        log.info("Get title for all the modules")
        indexes_to_remove = set()
        log.info("Get all module names and progress %")
        module_stats: list[tuple[str, str]] = [(module.find_element(By.TAG_NAME, "span").get_attribute("textContent"),
                         module.find_element(By.CLASS_NAME, "progress-bar").text)
                        for module in modules]
        log.info("Found %d modules", len(modules))
        for ind, (name, progress) in enumerate(module_stats):
            if name in course_ignore_list:
                log.info("%s in ignore list, index=%d", name, ind)
                indexes_to_remove.add(ind)
            elif "100" in progress:
                log.info("%s 100%% completed, index=%d", name, ind)
                indexes_to_remove.add(ind)
        log.info("Removing indexes %s", ", ".join(str(i) for i in indexes_to_remove))
        for ind in sorted(indexes_to_remove, reverse=True):
            modules.pop(ind)
            module_stats.pop(ind)
        log.info("Remaining modules %d", len(modules))
        log.info("Course at index 0")
        if len(modules):
            if module_stats[0][0] == current_course:
                log.info("Continue %s", current_course)
            else:
                current_course = module_stats[0][0]
                log.info("Start course %s", current_course)
        else:
            log.info("All modules visited on this page. Move to next page")
            next_btn: WebElement = driver.find_element(By.CSS_SELECTOR, "div.pagination span:last-child")
            if next_btn == ">":
                next_btn.click()
            else:
                log.info("All courses completed from all the pages. End the program.")
                all_done = True
            break
        log.info("Click Launch button")
        modules[0].find_element(By.CLASS_NAME, "butt").click()
        wait(driver, 15).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, "center .btn"))
        )
        sleep(1)
        log.info("Click Start Lesson button")
        driver.refresh()
        sleep(2)
        driver.find_element(By.CSS_SELECTOR, "center .btn").click()
        sleep(1)
        driver.switch_to.window(driver.window_handles[-1])
        log.info("Moved to %s window", driver.title)
        log.info("Check if element with class name 'moduleContent'")
        if driver.has_element(By.CLASS_NAME, "moduleContent"):
            log.info("Add %s to ignore list", current_course)
            course_ignore_list.add(current_course)
            log.info("Close current course and move to next course.")
            sleep(2)
            move_to_next_course()
            continue
        
        log.info("Element not present. Continue to course")
        break
    if all_done:
        break

driver.close()
process.kill()

from sys import exit
from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, InvalidElementStateException

options = Options()
options.add_experimental_option("debuggerAddress", "localhost:9876")
driver: WebDriver = Chrome(options=options,
                service=Service(executable_path="/home/abhishek/Documents/python/chromedriver"))

windows = driver.window_handles
driver.switch_to.window(windows[0])
frame = driver.find_element(By.CSS_SELECTOR, "iframe#textcontent")
driver.switch_to.frame(frame)
print(driver.find_element(By.CLASS_NAME, "textnote").text)

driver.switch_to.default_content()
print(driver.find_element(By.CLASS_NAME, "status").text.split(" of ", 1))
from sys import exit
from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, InvalidElementStateException

options = Options()
options.add_experimental_option("debuggerAddress", "localhost:9876")
driver: WebDriver = Chrome(options=options,
                service=Service(executable_path="/home/abhishek/Documents/python/chromedriver"))

windows = driver.window_handles
driver.switch_to.window(windows[0])
pages = driver.find_element(By.ID, "pageNum").text.split(" / ", 1)

tabs_done, pie_done, bulb_done, principle_done = False, False, False, False
sleep(5)
while pages[0] != pages[1]:
    try:
        # Click each part present
        click_here = driver.find_element(By.ID, "text_e")
        if click_here.is_displayed():
            click_here = click_here.text
            print(click_here)
            if "Next" in click_here or "next" in click_here:
                print("Found Next button to click")
                driver.find_element(By.ID, "continue1").click()
                sleep(4)
                continue
            elif "pie" in click_here and not pie_done:
                print("Found Pie to click")
                for ind, tab in enumerate(driver.find_elements(By.CSS_SELECTOR, '[id^="mybtn"]')):
                    print("Click pie", ind)
                    tab.click()
                    sleep(3)
                pie_done = True
            elif "tab" in click_here and not tabs_done:
                print("Found tabs to click")
                for ind, tab in enumerate(driver.find_elements(By.CSS_SELECTOR, '[id^="mybtn"]')):
                    print("Click tab", ind)
                    tab.click()
                    sleep(2)
                for ind, tab in enumerate(driver.find_elements(By.CSS_SELECTOR, '[class^="arw"]')):
                    print("Click tab", ind)
                    tab.click()
                    sleep(2)
                tabs_done = True
            elif "bulb" in click_here and not bulb_done:
                print("Found bulb to click")
                driver.find_element(By.CSS_SELECTOR, "a#btn1>img").click()
                bulb_done = True
            elif "principle" in click_here and not principle_done:
                print("Found principle to click")
                for ind, tab in enumerate(driver.find_elements(By.CSS_SELECTOR, '[id^="btn"]')):
                    print("Click tab", ind)
                    tab.click()
                    sleep(2)
                    driver.find_elements(By.CSS_SELECTOR, '[id^="btnn"]')[ind].click()
                    sleep(2)
                principle_done = True
    except (NoSuchElementException, InvalidElementStateException):
        pass
    try:
        if driver.find_element(By.ID, "click_nextcontinue").is_displayed():
            driver.find_element(By.ID, "next_btn").click()
            tabs_done, pie_done, bulb_done, principle_done = False, False, False, False
            print("Next it is")
            sleep(10)
    except NoSuchElementException:
        sleep(4)
    else:
        sleep(4)
    pages = driver.find_element(By.ID, "pageNum").text.split(" / ", 1)
    print(pages)

print("Initiate exit")
while True:
    if driver.find_element(By.ID, "click_nextcontinue").is_displayed():
        print("Click exit")
        driver.find_element(By.ID, "exit_btn").click()
        sleep(1)
        driver.find_element(By.ID, "onExitYes").click()
        break
    sleep(3)
